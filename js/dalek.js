Qt.include("model.js")
Qt.include("action.js")
Qt.include("helpers.js")



// Регистрация/Авторизация



// >> daemon
function init() {
    setTimeout(function() {
        daemon.call('connect', "127.0.0.1");
    }, 0);
}

// << daemon
function onConnect(status, info, register_type) {
    console.log("onConnect", register_type);
    welcomeActivity.visible = false;
    loginLayout.visible = true;

    root.registerType = register_type;
}

// << daemon
function onDaemonGetAccounts(json_accounts) {
    console.log("onDaemonGetAccounts", json_accounts);
    var accounts = JSON.parse(json_accounts);
    accounts.push("+ add user")

    root.user_accounts = accounts

}

// << daemon
function onAuthorize(status, info, json_data) {
    print("start qml onAuthorize");

    var login, conversation_data;

    welcomeActivity.visible = false;
    loginLayout.visible = false;
    pageLayout.visible = true;

    var data = JSON.parse(json_data);
    root.user = modelAccountData(data)
    accountData(data)

    for (login in data.contacts) {
        console.log("login", login);
        console.debug(login);
        if (login === "modtest") {
            login = "skaro::localhost/@modtest"
        }
        conversation_data = modelConversationData({'login': login})
        console.debug(conversation_data);
        addConversation(conversation_data)
    }

    daemon.call('getContacts');
}

// >> daemon
function authorize(login, password) {
    daemon.call('authorize', [login, password]);
}

// >> daemon
function register(email, login, password) {
    print("start register");
    daemon.call('startRegister', [email, login, password]);
}

// << daemon
function onRegister(status, info) {
    messages.displayMessage("Registration is completed successfully");

    daemon.call('authorize', [root.account_login, root.account_password]);
}



// Работа с контактами



// << daemon
function onGetContacts(status, info, json_data) {
    var i, user, conversation_data;

    console.log("onGetContacts", status);
    console.log(json_data);

    var data = JSON.parse(json_data);
    for (i in data) {
        console.log("i", i);
        console.log("data[i]", data[i]);
        user = data[i]
        console.log("user.login", user.login);
        conversation_data = modelConversationData(user)
        updateConversation(user.login, conversation_data)

        daemon.call('getHistory', [user.login]);
    }
}

// << daemon
function onAddContact(status, info, json_data) {
    var conversation_data;

    var data = JSON.parse(json_data);
    conversation_data = modelConversationData(data)
    addConversation(conversation_data)

}

// << daemon
function onDelContact(status, info, login){
    console.log("onDelContact");
    console.log(login);
    removeConversationByLogin(login);
    delete(chatPages[login]);
}

// >> daemon
function searchByLogin(login) {
    daemon.call('searchContacts', [login]);
}

// << daemon
function onSearchContacts(status, info, json_data) {
    var i, user, search_data;

    root.searchModel.clear();

    var data = JSON.parse(json_data);
    for (i in data) {
        root.searchModel.append(data[i]);
    }
}



// Работа с пользователями



// << daemon
function onGetUserData(status, info, json_data) {
    var conversation_data;
    console.log("onGetUserData");
    console.log(json_data);

    var data = JSON.parse(json_data);
    conversation_data = modelConversationData(data)
    updateConversation(data.login, conversation_data)
}

// << daemon
function onChangedUserData(login, json_data){
    var data = JSON.parse(json_data);
    updateConversation(login, data);
}



// Работа с сообщениями



// << daemon
function onGetHistory(status, info, login, json_data) {
    var i, mess, message_data;
    var data = JSON.parse(json_data);

    for (i in data) {
        mess = data[i];
        message_data = modelMessageData(mess)
        addConversationMessage(login, message_data, "bottom")
    }
}

// >> daemon
function sendMessage(login, message) {
    daemon.call('sendMessage', [login, message, false], function (marker) {

        var data = {
            mid: marker,
            from: root.user,
            body: message,
            status: "sending"
        }
        console.log("sendMessage, marker:", marker);
        var message_data = modelMessageData(data)
        addConversationMessage(login, message_data, "bottom")
    });
}

// << daemon
function onSendMessage(status, info, mid, date, login, marker) {
    console.log("onSendMessage, marker:", marker);
    var data = {
        'mid': mid,
        'date': date,
        'status': status,
        'info': info
    }
    var message_data = modelMessageData(data);
    editConversationMessage(marker, login, message_data)
}

// << daemon
function onReceivedMessage(mid, date, json_from, body) {
    var from = JSON.parse(json_from);

    var data = {
        'mid': mid,
        'date': date,
        'body': body,
        'status': "200 OK"
    }
    var message_data = modelMessageData(data)
    addConversationMessage(from.login, data, "bottom")
}



// Работа со своими данными



// >> daemon
function updateMyData(name) {
    var data = {'name': name};
    daemon.call('updateMyData', JSON.stringify(data));
}

// << daemon
function onUpdateMyData(status, info, json_data) {
    console.log('приняте изменений');
    var data = JSON.parse(json_data);
    accountData(data, true);
}



// Прочее



// << daemon
function onError(status, info) {
    messages.displayMessage("error: "+info);
}


// << daemon
function onInfo(status, info) {
    messages.displayMessage("info: "+info);
}











var setTyping = function(login, type) {
    // TODO
    console.log('typing...', type);
}
