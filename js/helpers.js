/* >>> Timers */
/* Реализация setTimeout() и clearTimeout() для QML */
function Timer() {
    return Qt.createQmlObject("import QtQuick 2.0; Timer {}", root);
}

var helpersTimerKeys = [];

function request_id() {
    if (helpersTimerKeys.length > 0) {
        var max_key = 1;
        for (var i=1; i<helpersTimerKeys.length; i++) {
            if (helpersTimerKeys[i] > max_key) {
                max_key = helpersTimerKeys[i];
            }
        }
        return max_key+1;
    } else {
        return 1;
    }
}

function setTimeout(callback, delay)
{
    var id = request_id();
    var timer = new Timer();
    timer.triggered.connect(function() {
        delete(helpersTimerKeys[id]);
        callback();
    });
    // note: an interval of 0 is directly triggered, so add a little padding
    timer.interval = delay + 1;
    timer.running = true;
    helpersTimerKeys[id] = timer;
    return id;
}

function clearTimeout(id) {
    helpersTimerKeys[id].running = false;
    delete(helpersTimerKeys[id]);
}

/* <<< Timers */



/* Эмуляция "запрос-ответ"
*
* function Request({
*     timeout: (int),
*     success: (callback),
*     error: (callback),
*     always: (callback),
*     data: somedata
* })
*/
function Request(s) {
    if (typeof s.timeout === "undefined") s.timeout = 10;
    if (typeof s.data !== "undefined") this.data = s.data;

    this.succes_do = s.success
    this.error_do = s.error
    this.always_do = s.always

    var self = this;
    this.success = function(request) {
        console.timeEnd("Query "+self.request_id);

        if (typeof self.succes_do !== "undefined") self.succes_do(request);
        if (typeof self.always_do !== "undefined") self.always_do("success", request);

        clearTimeout(self.request_id);
        delete LibSkaro.requests[self.request_id];
    }
    this.error = function(code) {
        console.timeEnd("Query "+self.request_id);

        if (typeof self.error_do !== "undefined") self.error_do(code);
        if (typeof self.always_do !== "undefined") self.always_do("error", code);

        clearTimeout(self.request_id);
        delete LibSkaro.requests[self.request_id];
    }

    this.request_id = setTimeout(self.error,s.timeout*1000);
    LibSkaro.requests[this.request_id] = this;

    console.time("Query "+self.request_id);
}


/*
* Returns a random integer between min (included) and max (excluded)
* Using Math.round() will give you a non-uniform distribution!
*/
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}


/*
* Recursively merge properties of two objects
*/
function mergeRecursive(obj1, obj2) {

  for (var p in obj2) {
    try {
      // Property in destination object set; update its value.
      if ( obj2[p].constructor === Object ) {
        obj1[p] = mergeRecursive(obj1[p], obj2[p]);

      } else {
        obj1[p] = obj2[p];

      }

    } catch(e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];

    }
  }

  return obj1;
}
