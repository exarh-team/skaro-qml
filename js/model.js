function getConversationName(data) {
    var name = data.name
    if ((name === undefined)||(name === "")) {
        name = data.login;
        if (name.search("@") !== -1) {
            name = name.split("@")[1]
        }
    }
    return name
}

function modelConversationData(raw) {
    return {
        login: (("login" in raw) ? raw.login : "Anonymous"),
        name: getConversationName(raw),
        status_message: "",
        icon: "",
        unread_count: false,
        status: (("status" in raw) ? raw.status : "offline"),
        last_mess_date: "",
        last_mess_body: "",
        current_info: (("current_info" in raw) ? raw.current_info : ""),
        loaded: false
    }
}

function modelAccountData(raw) {
    return {
        login: raw.login,
        name: raw.name,
        current_info: (("current_info" in raw) ? raw.current_info : '')
    }
}

function modelMessageData(raw) {
    var login, name;

    if ("login" in raw) {
        login = raw.login;
        name = raw.name;
    } else if (("from" in raw)&&("login" in raw.from)) {
        login = raw.from.login;
        name = raw.from.name;
    } else {
        login = "Anonymous";
        name = "Anonymous";
    }

    return {
        mid: raw.mid,
        login: login,
        name: name,
        body: raw.body,
        date: (("date" in raw) ? raw.date : ''),
        status: (("status" in raw) ? raw.status : 'ok')
    }
}
