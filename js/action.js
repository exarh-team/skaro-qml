function accountData(raw, update) {
    var data
    if (update) {
        data = mergeRecursive(usersData[root.account_login], raw)
    } else {
        data = raw
    }

    root.account_login = data.login

    console.log("accountData: ", root.account_login);
    root.account_password = ""

    var tmp = usersData
    tmp[data.login] = data
    usersData = tmp
    delete tmp;
}


var realMerge = function (to, from) {
    for (n in from) {
        if (typeof to[n] != 'object') {
            to[n] = from[n];
        } else if (typeof from[n] == 'object') {
            to[n] = realMerge(to[n], from[n]);
        }
    }

    return to;
};

function addConversation(data) {
    root.conversationsModel.append(data);

    var tmp = usersData;
    tmp[data.login] = data;
    console.log("addConversation", data.login)
    usersData = tmp;
    delete tmp;

    var chatModel = chatModelComponent.createObject(root);
    chatModels[data.login] = chatModel;

    var chatPage = chatPageComponent.createObject(
        null,
        {
            mLogin: data.login,
            chatModel: chatModel
        }
    );
    chatPages[data.login] = chatPage;
}

function updateConversation(login, data) {
    console.log(login);
    var tmp = usersData;
    if (data.name !== undefined) {
        tmp[login].name = data.name;
    }
    if (data.status !== undefined) {
        tmp[login].status = data.status;
    }
    if (data.current_info !== undefined) {
        tmp[login].current_info = data.current_info;
    }
    usersData = tmp;
    delete tmp;

}

function addConversationMessage(dialog, data, insert_mode) {
    var chatPage = chatPages[dialog];

    if (insert_mode === "top") {
        chatModels[dialog].insert(0, data);
    }
    else if (insert_mode === "bottom") {
        var countMessages = chatModels[dialog].count
        if (countMessages > 0) {
            data.sameUser = chatModels[dialog].get(countMessages-1).login === data.login
        } else {
            data.sameUser = false
        }

        chatModels[dialog].append(data);

        var id = getConversationModelIndexByLogin(dialog);
        conversationsModel.get(id).last_mess_date = data.date;
        conversationsModel.get(id).last_mess_body = data.body.replace(/\n/g," ");

        if (chatPage.listView.isAtBottomArea)
            chatPage.listView.positionViewAtEnd();

        sortConversationsModel();
    }
}

function editConversationMessage(marker, dialog, data) {
    debugEvent('edit-conversation-message to ', dialog)

    var chatPage = chatPages[dialog];

    var id = getMessageModelIndexByMID(marker, dialog);
    console.log(id);
    chatModels[dialog].get(id).mid = data.mid;
    chatModels[dialog].get(id).date = data.date;
    if (data.body !== undefined)
        chatModels[dialog].get(id).body = data.body;
    chatModels[dialog].get(id).status = data.status;

    var id = getConversationModelIndexByLogin(dialog);
    conversationsModel.get(id).last_mess_date = data.date;
    if (data.body !== undefined)
        conversationsModel.get(id).last_mess_body = data.body.replace(/\n/g," ");

    sortConversationsModel();
};

