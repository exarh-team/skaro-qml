#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "chelpers.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;


    CHelpers helpers;
    QQmlContext *context = engine.rootContext();    // Создаём корневой контекст
    /* Загружаем объект в контекст для установки соединения,
     * а также определяем имя, по которому будет происходить соединение
     * */
    context->setContextProperty("chelpers", &helpers);


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
