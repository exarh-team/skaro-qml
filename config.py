#!/usr/bin/env python

import os

import yaml  # PyYAML


def read_all():
    with open(conf_path) as json_file:
        try:
            json_data = yaml.load(json_file)
            if json_data is None:
                json_data = {}
        except yaml.YAMLError:
            json_data = {}
    return json_data


def read(key):
    if key in config:
        return config[key]
    else:
        return


def write(key, data):
    config[key] = data
    f = open(conf_path, 'w')
    f.write(yaml.dump(config))


if __name__ != "__main__":
    path = os.path.expanduser('~')
    if os.name == 'nt':
        path += '/Application Data'
    else:
        path += '/.config'

    if not os.path.exists(path):
        os.mkdir(path)

    path += '/skaro-qml'

    if not os.path.exists(path):
        os.mkdir(path)

    conf_path = path + '/default.yaml'

    if os.path.exists(conf_path):
        config = read_all()
    else:
        config = {}
        open(conf_path, 'tw', encoding='utf-8')
