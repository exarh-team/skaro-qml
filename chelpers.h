#ifndef CHELPERS_H
#define CHELPERS_H

#include <QObject>

class CHelpers : public QObject
{
    Q_OBJECT
public:
    explicit CHelpers(QObject *parent = 0);

signals:
    // Сигнал для передачи данных в qml-интерфейс
    void responsePasswordHash(QString login, QString hash);

public slots:
    // Слот для приёма данных из qml-интерфейса
    void requestPasswordHash(QString, QString, QString);

};

#endif // CHELPERS_H
