# Skaro-QML client for protocol skaro

An official skaro client for everything.

## Installation on Debian

### Dependencies
* Qt >= 5.8

### Install and run on Debian

Clone Skaro-QML

```
git clone https://gitlab.com/exarh-team/skaro-qml.git
```

## Technical details

The app is based on the Qt framework, and written in C++, JavaScript and QML.

### Third party libraries

Besides the frameworks mentioned above, other libraries are used for protocol and UI needs. Here is the short list:

* [cbor-js](https://github.com/paroga/cbor-js)
* [PupSubJS](https://github.com/mroderick/PubSubJS)
* [Google Material Icons](https://google.github.io/material-design-icons/)

### Licensing

The source code is licensed under GPL v3. License is available [here](/LICENSE).
