import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtGraphicalEffects 1.0
import QtWebSockets 1.1
import Qt.labs.settings 1.0
import org.nemomobile.dbus 2.0

import "qrc:/ui"

import "qrc:/js/dalek.js" as Dalek

ApplicationWindow {
    visible: true
    width: 480
    height: 640
    title: qsTr("Skaro")

    id: root

    property ListModel conversationsModel: ListModel {}
    property ListModel searchModel: ListModel {}
    property var chatPages: ({})
    property var chatModels: ({})
    property var usersData: ({})
    property bool debugEvents: false

    property string registerType: "open"
    property string account_login
    property string account_password
    property string account_token
    property var user
    property var user_accounts

    property bool isSearch: false
    property bool isMobile: root.width<root.height
    property int opacitySearchTime: 50

    property var statusColors: {
        "offline": "#00000000",
        "online": "#67BA00"
    }

    signal setTyping (string login, string type)
    signal sendMessage (string login, string data)
    signal searchByLogin (string login)
    signal register (string email, string login, string password)
    signal updateMyData (string name)

    onSetTyping: {
        Dalek.setTyping(login, type)
    }

    onSendMessage: {
        Dalek.sendMessage(login, data)
    }

    onSearchByLogin: {
        Dalek.searchByLogin(login)
    }

    onRegister: {
        Dalek.register(email, login, password)
    }

    onUpdateMyData: {
        Dalek.updateMyData(name)
    }


    function getConversationModelIndexByLogin(login) {
        for (var i=0; i<conversationsModel.count; i++) {
            if (conversationsModel.get(i).login === login) {
                return i;
            }
        }
        return false;
    }

    function removeConversationByLogin(login) {
        var id = getConversationModelIndexByLogin(login);
        if (id !== false) {
            conversationsModel.remove(id, 1);
        }
    }

    function getMessageModelIndexByMID(mid, dialog) {
        for (var i=0; i<chatModels[dialog].count; i++) {
            if (chatModels[dialog].get(i).mid === mid) {
                return i;
            }
        }
        return false;
    }

    function sortConversationsModel() {
        var n;
        var i;
        for (n=0; n < conversationsModel.count; n++)
            for (i=n+1; i < conversationsModel.count; i++) {
                if (conversationsModel.get(n).last_mess_date < conversationsModel.get(i).last_mess_date) {
                    conversationsModel.move(i, n, 1);
                    n=0;
                }
            }
    }



    header: ToolBarComponent {
        id: toolBarId
    }

    Component {
        id: chatModelComponent
        ListModel {}
    }

    Component {
        id: chatPageComponent
        DialogFormActivity {}
    }

    function debugEvent() {
        if (debugEvents) {
            var s = "";
            for (var a=0; a<arguments.length; a++)
                s += arguments[a] + (a < arguments.length-1 ? " " : "")
            console.debug(s);
        }
    }

    WelcomeActivity {
        id: welcomeActivity
        visible: true
    }
    Item {
        id: loginLayout
        anchors.fill: parent
        visible: false

        StackView {
            id: loginView
            anchors.fill: parent

            initialItem: Item {
                width: parent.width
                height: parent.height

                LoginFormActivity {
                    id: loginFormActivity
                }
            }
        }
    }

    Item {
        anchors.fill: parent

        RowLayout {
            anchors.fill: parent

            ContactListActivity {
                Layout.preferredWidth: 300
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: undefined
                width: 300
                visible: (root.width>root.height)
            }

            Item {
                id: pageLayout
                visible: false
                Layout.fillWidth: true
                Layout.fillHeight: true

                StackView {
                    id: pageView
                    anchors.fill: parent

                    initialItem: Item {
                        width: parent.width
                        height: parent.height

                        ContactListActivity {
                            anchors.fill: parent
                        }
                    }
                }
            }
        }
    }

    InfoBanner {
        id: messages
    }

    Drawer {
        id: drawer
        width: root.width-56
        height: root.height

        DrawerComponent {}
    }

    Settings {
        id: settings
        property string session_id: ""
    }

    DBusInterface {
        id: daemon

        service: 'im.skaro.Daemon'
        iface: 'im.skaro.DaemonInterface'
        path: '/API'
        signalsEnabled: true

        function onConnect(status, info, register_type) { Dalek.onConnect(status, info, register_type) }
        function onDaemonGetAccounts(accounts) { Dalek.onDaemonGetAccounts(accounts) }
        function onAuthorize(status, info, data) { Dalek.onAuthorize(status, info, data) }
        function onRegister(status, info) { Dalek.onRegister(status, info) }

        function onGetContacts(status, info, data) { Dalek.onGetContacts(status, info, data) }
        function onAddContact(status, info, data) { Dalek.onAddContact(status, info, data) }
        function onDelContact(status, info, login) { Dalek.onDelContact(status, info, login) }
        function onSearchContacts(status, info, data) { Dalek.onSearchContacts(status, info, data) }

        function onGetUserData(status, info, data) { Dalek.onGetUserData(status, info, data) }
        function onChangedUserData(login, data) { Dalek.onChangedUserData(login, data) }

        function onGetHistory(status, info, login, data) { Dalek.onGetHistory(status, info, login, data) }
        function onSendMessage(status, info, mid, date, login, marker) { Dalek.onSendMessage(status, info, mid, date, login, marker) }
        function onReceivedMessage(mid, date, from, body) { Dalek.onReceivedMessage(mid, date, from, body) }

        function onUpdateMyData(status, info, records) { Dalek.onUpdateMyData(status, info, records) }

        function onError(status, info) { Dalek.onError(status, info) }
        function onInfo(status, info) { Dalek.onInfo(status, info) }

        Component.onCompleted: {
            Dalek.init();
        }
    }

}
