#include <QMessageAuthenticationCode>
#include "chelpers.h"

CHelpers::CHelpers(QObject *parent) : QObject(parent)
{
}

void CHelpers::requestPasswordHash(QString login, QString token, QString password)
{
    QString hash = QMessageAuthenticationCode::hash(
                token.toLocal8Bit(),
                password.toLocal8Bit(),
                QCryptographicHash::Sha512).toHex();
    emit responsePasswordHash(login, hash);
}
