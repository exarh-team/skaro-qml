import QtQuick 2.4

Item {
    id: banner

    property alias message : messageText.text

    height: 10

    Rectangle {
        id: background

        anchors.fill: banner
        color: "black"
        smooth: true
        opacity: 0.8
    }

    Text {
        x: 9
        y: 9
        font.pixelSize: 12
        renderType: Text.QtRendering
        width: parent.width-20

        id: messageText

        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        color: "white"
    }

    states: State {
        name: "portrait"
        PropertyChanges { target: banner; height: 35 }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            messages.state = ""
        }
    }
}
