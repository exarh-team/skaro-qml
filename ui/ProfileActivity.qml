import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.1
import QtGraphicalEffects 1.0

Item {
    property string mLogin: root.account_login

    property string defaultName: usersData[mLogin].name

    property bool edit_mode: false

    Rectangle {
        anchors.fill: parent

        color: "#eee"

        Rectangle {
            id: dialog_top
            color: "#4a4f65"
            width: parent.width
            height: 35

            Image {
                id: arrowBackIcon
                visible: false
                source: Qt.resolvedUrl("../media/google-md/navigation-arrow-back.svg")
                width: 24
                height: 24
                sourceSize: Qt.size(width, height)
            }

            ColorOverlay {
                x: 5
                y: 5
                width: arrowBackIcon.width
                height: arrowBackIcon.height
                source: arrowBackIcon
                color: "#fff"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        pageView.pop()
                    }
                }
            }

        }

        Rectangle {
            id: top_area
            y: 35
            width: parent.width
            height: 64
            color: "#8888ff"

            Rectangle {
                width: parent.width;
                height: 45
                color: "#9a9aff"
                visible: edit_mode
            }

            TextField {
                id: pf_name
                x: parent.height-2
                y: 5
                height: 42
                width: parent.width-parent.height

                text: usersData[mLogin].name
                readOnly: !edit_mode

                style: TextFieldStyle {
                    textColor: "#eee"
                    background: Rectangle {
                        color: "#00000000"
                    }
                    font.pixelSize: 32
                }
            }

            Text {
                text: usersData[mLogin].login
                x: parent.height+12
                y: 40
                color: "#fff"
            }

            AvatarBase {
                width: parent.height
                height: parent.height
                text: usersData[mLogin].name
            }

            Rectangle {
                color: "red"
                x: parent.width-30
                width: 30
                height: 30

                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        pageView.pop();
                        daemon.call('delContact', [mLogin]);
                    }
                }
            }
        }

        Rectangle {
            y: 100
            width: parent.width
            color: "#fff"
            visible: (usersData[mLogin].current_info !== '')

            Text {
                id: current_info
                text: usersData[mLogin].current_info
                x: 5
                y: 5
                wrapMode: Text.Wrap
                color: "#333"
            }

            height: current_info.contentHeight+10
        }
    }

    Rectangle {
        x: parent.width-75
        y: parent.height-75
        width: 50
        height: 50
        color: "#F44336"
        visible: (mLogin==account_login)

        Image {
            id: imageEditIcon
            visible: false
            source: Qt.resolvedUrl("../media/google-md/image-edit.svg")
            width: 32
            height: 32
            sourceSize: Qt.size(width, height)
        }

        ColorOverlay {
            x: 9
            y: 9
            width: imageEditIcon.width
            height: imageEditIcon.height
            source: imageEditIcon
            color: "#fff"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (edit_mode && defaultName !== pf_name.text) {
                        root.updateMyData(pf_name.text)
                    }
                    edit_mode = !edit_mode
                }
            }
        }

    }

}
