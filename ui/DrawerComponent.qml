import QtQuick 2.4
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3

Item {
    anchors.fill: parent

    Component {
        id: profileActivityComponent
        ProfileActivity {}
    }

    Rectangle {
        width: parent.width
        height: 64
        color: "#8888ff"

        AvatarBase {
            width: parent.height
            height: parent.height
            text: account_login
        }

        Text {
            text: (usersData[account_login]) ? usersData[account_login].name : ""
            x: parent.height+12
            width: parent.width-x
            color: "#fff"
            font.pixelSize: 32
            elide: Text.ElideRight
        }

        Text {
            text: account_login
            x: parent.height+12
            y: 40
            width: parent.width-x
            color: "#fff"
            elide: Text.ElideRight
        }

        MouseArea {
            anchors.fill: parent

            onClicked: {
                var profileActivity = profileActivityComponent.createObject(
                    null
                );
                onClicked: pageView.push(profileActivity);
                // contactList.state = "";
            }
        }
    }

    ColumnLayout {
        y: 64
        spacing: 0

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 32

            Text {
                text: "Статус"
                x: 10
                y: 10
                color: "#111"
                font.pixelSize: 16
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 32

            Text {
                text: "Контакты"
                x: 10
                y: 10
                color: "#111"
                font.pixelSize: 16
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 32

            Text {
                text: "Отметить все как прочитанные"
                x: 10
                y: 10
                color: "#111"
                font.pixelSize: 16
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 32

            Text {
                text: "Послать инвайт"
                x: 10
                y: 10
                color: "#111"
                font.pixelSize: 16
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 32

            Text {
                text: "Импорт / экспорт"
                x: 10
                y: 10
                color: "#111"
                font.pixelSize: 16
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 32

            Text {
                text: "Настройки"
                x: 10
                y: 10
                color: "#111"
                font.pixelSize: 16
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 32

            Text {
                text: "Помощь"
                x: 10
                y: 10
                color: "#111"
                font.pixelSize: 16
            }
        }
    }

}
