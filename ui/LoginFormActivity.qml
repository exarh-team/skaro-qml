import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

import "qrc:/js/dalek.js" as Dalek

Item {

    Component {
        id: registerFormComponent
        RegisterFormActivity {}
    }

    ColumnLayout {
        y: (root.height-height)/2
        x: (root.width-root.width/15 < 300) ? root.width/30 : root.width/2-width/2
        width: (root.width-root.width/15 < 300) ? root.width-root.width/15 : 300

        Image {
            id: logo
            source: Qt.resolvedUrl("../media/logo.svg")
            width: (root.width < 200) ? root.width : 200
            height: (root.width < 200) ? root.width : 200
            sourceSize: Qt.size(width, height)
            Layout.alignment: Qt.AlignHCenter
        }

        Item {
            height: 20
        }

        ComboBox {
            id: lf_login
            model: root.user_accounts
            Layout.fillWidth: true
        }

//        TextField {
//            id: lf_login
//            placeholderText: qsTr("User name")
//            Layout.fillWidth: true
//        }

        TextField {
            placeholderText: qsTr("Password")
            id: lf_password
            echoMode: TextInput.Password
            Layout.fillWidth: true
        }

        Button {
            text: "Log in"
            highlighted: true
            Layout.fillWidth: true

            onClicked: {
                Dalek.authorize(lf_login.currentText, lf_password.text);
            }
        }
        // Keys.onPressed: {
        //     if ( event.key === Qt.Key_Enter || event.key === Qt.Key_Return ) {
        //        py.call('backend.authorize', [lf_login.text, lf_password.text])
        //     }
        // }

        Button {
            visible: (registerType === "close") ? false : true
            Layout.fillWidth: true

            text: "Register"

            onClicked: {
                var registerModel = registerFormComponent.createObject(root);
                loginView.push(registerModel)
            }
        }


    }
}
