import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: component

    property var tabs : []

    property int count : tabs.length
    property int currentIndex: 0

    property color backgroundColor: "#fff"
    property color colorActive: "#00796b"
    property color colorInactive: "#636363"
    property color borderColor: "#e4e4e4"

    height: _titleAndIcon ? 56 : 48

    property bool _titleAndIcon: false
    property int _itemWidth: component.width / component.count

    DropShadow {
        id: rectShadow
        radius: 8.0
        samples: 8
        color: Qt.rgba(0, 0, 0, 0.13)
        anchors.centerIn: component
        width:  component.width  + (2 * rectShadow.radius)
        height: component.height
    }

    Rectangle {
        anchors.centerIn: component
        width:  component.width  + (4 * rectShadow.radius)
        height: component.height
        color: backgroundColor
    }

    Row {
        anchors.fill: parent

        Repeater {
            id: repeater

            model: component.tabs
            delegate: BottomNavigationButton {
                title: modelData.title ? modelData.title : ""
                iconSource: modelData.iconSource ? modelData.iconSource : ""
                width: component._itemWidth
                height: component.height
                active: currentIndex === index

                onClicked: {
                    component.currentIndex = index;
                }

                Component.onCompleted: {
                    if (title !=="" && iconSource !=="")
                        component._titleAndIcon = true;
                }
            }
        }
    }

    Rectangle {
            id: border
            height: 1
            width: component.width
            color: borderColor
            anchors.top: parent.top
    }

}
