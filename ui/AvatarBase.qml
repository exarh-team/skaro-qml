import QtQuick 2.4

Rectangle {
    property string text
    color: getIconColor(text, colors)

    property var colors: ["#6b9362", "#4d8fac", "#bb7796", "#b95754", "#ffa565", "#f7bb7d", "#bda928"]

    function getIconColor(text, colors) {
        /*
           Code derived from Dekko (https://launchpad.net/dekko)
           Thanks to Dan Chapman.
        */
        var tmp = 0;
        for (var i = 0; i < text.length; i++) {
            tmp += text.charCodeAt(i);
        }
        return colors[tmp % colors.length];
    }
}
