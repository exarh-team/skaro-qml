import QtQuick 2.0

Rectangle {
    id: component

    property var tabs : []

    property int count : tabs.length
    property int currentIndex: 0

    property color colorActive: "#fff"
    property color colorInactive: "#353637"

    height: _titleAndIcon ? 56 : 48

    property bool _titleAndIcon: false
    property int _itemWidth: component.width / component.count

    color: colorInactive
    width: parent.width

    Row {
        anchors.fill: parent

        Repeater {
            id: repeater

            model: component.tabs
            delegate: BottomNavigationButton {
                title: modelData.title ? modelData.title : ""
                iconSource: modelData.iconSource ? modelData.iconSource : ""
                width: component._itemWidth
                height: component.height
                active: currentIndex === index

                onClicked: {
                    component.currentIndex = index;
                }

                Component.onCompleted: {
                    if (title !=="" && iconSource !=="")
                        component._titleAndIcon = true;
                }
            }
        }
    }

}
