import QtQuick 2.4
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import "time.js" as Time

Item {
    id: contactList

    Component {
        id: profileActivityComponent
        ProfileActivity {}
    }

    Rectangle {
        width: parent.width
        height: parent.height

        Rectangle {
            color: "black"
            width: parent.width
            height: 35
            opacity: isSearch
            z: 10

            RowLayout {
                width: parent.width
                spacing: 0

                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "red"

                    Text {
                        y: 8
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Global"
                        color: "#fff"
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Text {
                        y: 8
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Contacts"
                        color: "#fff"
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Text {
                        y: 8
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Messages"
                        color: "#fff"
                    }
                }
            }

        }

        // список собеседников
        ListView {
            width: parent.width
            height: parent.height-35
            spacing: 1
            opacity: !isSearch

            model: conversationsModel
            delegate:
                Item {
                    width: parent.width
                    height: 60

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            pageView.push(chatPages[login])
                            toolBarId.selectDialog = login
                        }
                    }

                    Item {
                        x: 16
                        y: 8
                        width: parent.width-x*2
                        height: parent.height-y*2

                        // место под аватарку
                        AvatarBase {
                            width: parent.height
                            height: parent.height
                            text: usersData[login].name
                            radius: width/2

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    var profileActivity = profileActivityComponent.createObject(
                                        null,
                                        {
                                            mLogin: login
                                        }
                                    );
                                    daemon.call('getUserData', [login]);
                                    onClicked: pageView.push(profileActivity)
                                }
                            }
                        }

                        Item {
                            width: parent.width-parent.height-16
                            height: parent.height
                            x: parent.height+16

                            // статус собеседника
                            Rectangle {
                                width: 3
                                height: parent.height
                                color: statusColors[usersData[login].status]
                            }

                            // имя собеседника
                            Label {
                                text: usersData[login].name
                                color: "#000"
                                opacity: 0.87
                            }

                            // время последнего сообщения
                            Label {
                                x: parent.width-width
                                width: 20
                                horizontalAlignment: Text.AlignRight
                                text: Time.getDate(last_mess_date)
                                color: "#000"
                                opacity: 0.54
                            }

                            // текст последнего сообщения
                            Label {
                                y: 21
                                width: parent.width
                                elide: Text.ElideRight
                                text: last_mess_body
                                color: "#000"
                                opacity: 0.54
                            }

                        }

                    }

                }
        }

        // результаты поиска
        ListView {
            y: 35
            width: parent.width
            height: parent.height-70
            spacing: 1
            visible: isSearch

            model: searchModel
            delegate:
                Rectangle {
                    width: parent.width
                    height: 50

                    Rectangle {
                        width: parent.width-parent.height
                        height: parent.height
                        x: parent.height
                        color: "#565C72"

                        Text {
                            x: 10
                            y: 6
                            color: "#A1A1A1"
                            text: login
                        }
                    }
                    Rectangle {
                        width: 30;
                        height: 30;
                        color: "#777777"
                        x: parent.width-30
                        y: 0

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                daemon.call('addContact', [login]);
                            }
                        }
                    }
                }
        }

    }

    property var colorModel: [
        { title: qsTr("Recents"), iconSource: Qt.resolvedUrl("../media/google-md/action-update.svg") },
        { title: qsTr("Broadcast"), iconSource: Qt.resolvedUrl("../media/google-md/action-announcement.svg") },
        { title: qsTr("Roster"), iconSource: Qt.resolvedUrl("../media/google-md/av-recent-actors.svg") }
    ]

    BottomNavigation {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        z: 10
        opacity: !isSearch
        tabs: colorModel
    }

    transitions: Transition {
        AnchorAnimation { easing.type: Easing.OutQuart; duration: 300 }
        NumberAnimation { property: "opacity"; duration: 300}
    }

}
