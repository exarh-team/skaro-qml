import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtGraphicalEffects 1.0

Item {

    property string mLogin
    property alias listView: listView
    property alias chatModel: listView.model

    Text {
        id: qqwweeg
        anchors.centerIn: parent
        text: parent.width + 'x' + parent.height
    }

    Rectangle {
        anchors.fill: parent
        color: "#eee"

        ListView {
            id: listView
            //anchors.fill: parent
            width: parent.width
            height: parent.height-bottomContainer.height
            spacing: 0

            property bool isAtBottomArea: contentHeight*(1-(listView.visibleArea.yPosition + listView.visibleArea.heightRatio)) < listView.height

            delegate:
                Item {
                    id: messageBlock

                    property int id: getConversationModelIndexByLogin(login)
                    property string name: conversationsModel.get(id).name
                    property bool sentByMe: login === account_login

                    anchors.right: sentByMe ? parent.right : undefined
                    anchors.left: !sentByMe ? parent.left : undefined
                    anchors.leftMargin: !sentByMe ? listView.width/15 : undefined
                    anchors.rightMargin: sentByMe ? listView.width/15 : undefined

                    AvatarBase {
                        y: 10
                        width: 36
                        height: 36
                        text: sentByMe ? usersData[account_login].name : usersData[login].name
                        radius: width/2
                        visible: !sentByMe
                    }

                    Item {
                        x: sentByMe ? 0 : 46
                        y: 10

                        Rectangle {
                            width: mess_body.width+12
                            height: mess_body.height+12
                            anchors.right: sentByMe ? parent.right : undefined
                            radius: 5
                            color: sentByMe ? "#efffde" : "#fff"
                            border.color: sentByMe ? "#b6d4ad" : "#ccd5dd"
                        }

                        ColumnLayout {
                            x: 6
                            y: 6
                            anchors.right: sentByMe ? parent.right : undefined
                            width: parent.width
                            spacing: 10

                            Label {
                                id: mess_body
                                wrapMode: Text.Wrap
                                color: "#333"
                                text: body ? body : ""
                                anchors.right: sentByMe ? parent.right : undefined
                                anchors.rightMargin: sentByMe ? 6 : undefined
                                Layout.maximumWidth: listView.width-listView.width/5-36
                            }

                            Label {
                                anchors.right: sentByMe ? parent.right : undefined
                                color: "#818181"
                                text: (sentByMe ? usersData[account_login].name : name) + " • " + (date ? date : "")
                                font.pixelSize: 12
                            }
                        }
                    }
                    height: mess_body.height+50
            }


            ScrollBar.vertical: ScrollBar {

            }
        }


        Rectangle {
            id: bottomContainer
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: 70
            color: "#464C62"

            Rectangle {
                width: parent.width
                height: 35
                color: parent.color

                Image {
                    id: insertEmoticonIcon
                    visible: false
                    source: Qt.resolvedUrl("../media/google-md/editor-insert-emoticon.svg")
                    width: 24
                    height: 24
                    sourceSize: Qt.size(width, height)
                }
                ColorOverlay {
                    x: 5
                    y: 5
                    width: insertEmoticonIcon.width
                    height: insertEmoticonIcon.height
                    source: insertEmoticonIcon
                    color: "#fff"
                }

                Image {
                    id: cameraAltIcon
                    visible: false
                    source: Qt.resolvedUrl("../media/google-md/image-camera-alt.svg")
                    width: 24
                    height: 24
                    sourceSize: Qt.size(width, height)
                }
                ColorOverlay {
                    x: 35
                    y: 5
                    width: cameraAltIcon.width
                    height: cameraAltIcon.height
                    source: cameraAltIcon
                    color: "#fff"
                }

                Image {
                    id: photoIcon
                    visible: false
                    source: Qt.resolvedUrl("../media/google-md/image-photo.svg")
                    width: 24
                    height: 24
                    sourceSize: Qt.size(width, height)
                }
                ColorOverlay {
                    x: 70
                    y: 5
                    width: photoIcon.width
                    height: photoIcon.height
                    source: photoIcon
                    color: "#fff"
                }

                Image {
                    id: attachFileIcon
                    visible: false
                    source: Qt.resolvedUrl("../media/google-md/editor-attach-file.svg")
                    width: 24
                    height: 24
                    sourceSize: Qt.size(width, height)
                }
                ColorOverlay {
                    x: 105
                    y: 5
                    width: attachFileIcon.width
                    height: attachFileIcon.height
                    source: attachFileIcon
                    color: "#fff"
                }

                Image {
                    id: micIcon
                    visible: false
                    source: Qt.resolvedUrl("../media/google-md/av-mic.svg")
                    width: 24
                    height: 24
                    sourceSize: Qt.size(width, height)
                }
                ColorOverlay {
                    x: 140
                    y: 5
                    width: micIcon.width
                    height: micIcon.height
                    source: micIcon
                    color: "#fff"
                }

                Image {
                    id: videoCallIcon
                    visible: false
                    source: Qt.resolvedUrl("../media/google-md/av-video-call.svg")
                    width: 24
                    height: 24
                    sourceSize: Qt.size(width, height)
                }
                ColorOverlay {
                    x: 175
                    y: 5
                    width: videoCallIcon.width
                    height: videoCallIcon.height
                    source: videoCallIcon
                    color: "#fff"
                }
            }

            Rectangle {
                y: 25
                width: parent.width
                height: 50
                color: parent.color

                TextArea {
                    id: messageField
                    height: parent.height
                    width: parent.width-30


                    onEditingFinished: {
                        if (messageField.text !== "") {
                            onTriggered: root.setTyping(mLogin, "stopped")
                            pausedTypingTimer.stop();
                            stoppedTypingTimer.stop();
                        }
                    }

                    Timer {
                        id: pausedTypingTimer
                        interval: 1500
                        onTriggered: {
                            root.setTyping(mLogin, "paused")
                            stoppedTypingTimer.start()
                        }
                    }

                    Timer {
                        id: stoppedTypingTimer
                        interval: 3000
                        onTriggered: root.setTyping(mLogin, "stopped")
                    }

                    onTextChanged: {
                        if (messageField.text !== "") {
                            root.setTyping(mLogin, "typing");
                            pausedTypingTimer.stop();
                            stoppedTypingTimer.stop();
                            pausedTypingTimer.start();
                        }
                    }
                }

                Image {
                    id: sendIcon
                    visible: false
                    source: Qt.resolvedUrl("../media/google-md/content-send.svg")
                    width: 24
                    height: 24
                    sourceSize: Qt.size(width, height)
                }

                ColorOverlay {
                    x: bottomContainer.width-height-3
                    y: 5
                    width: sendIcon.width
                    height: sendIcon.height
                    source: sendIcon
                    color: "#fff"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            Qt.inputMethod.commit();
                            Qt.inputMethod.hide();
                            if (messageField.text !== "") {
                                root.setTyping(mLogin, "stopped");
                                pausedTypingTimer.stop();
                                stoppedTypingTimer.stop();
                                root.sendMessage(mLogin, messageField.text);
                                messageField.cursorPosition = 0;
                                messageField.text = "";
                            }
                        }

                    }
                }
            }
        }
    }
}
