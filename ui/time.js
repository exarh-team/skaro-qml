// Выводит двузначное число с ведущим нулем
function zeroPlus(x) {
    if (x < 10)
        x = '0' + x;
    return x;
}
// Переводит в 12 часовой формат
function toAM(x) {
    if (x > 12)
        x -= 12;
    return x;
}

// Аналог функции date() из PHP
function parseDateFormat(format, Time) {
    var DateInFormat = '';
    if (format.length === 0)
        return;
    for (var i = 0; i < format.length; i++) {
        switch (format[i]) {
            // Часы
            // 12 часовой
            case 'g' : DateInFormat += toAM(Time.getHours()); break; // без ведущего нуля
            case 'h' : DateInFormat += zeroPlus(toAM(Time.getHours())); break; // C ведущим нулем
            // 24 часовой
            case 'G' : DateInFormat += Time.getHours(); break; // без ведущего нуля
            case 'H' : DateInFormat += zeroPlus(Time.getHours()); break; // с ведущим нулём
            // Годы
            case 'Y' : DateInFormat += Time.getFullYear(); break; // Четыре цифры
            case 'y' : DateInFormat += String(Time.getFullYear()).substr(2); break; // Две цифры
            // Месяцы
            case 'm' : DateInFormat += zeroPlus(Time.getMonth() + 1); break; //Порядковый номер месяца с ведущим нулём
            case 'n' : DateInFormat += Time.getMonth() + 1; break; // Порядковый номер месяца без ведущего нуля
                // case 'F' : DateInFormat += Lang.NameMonths[Time.getMonth()]; break; // Полное наименование месяца
                // case 'M' : DateInFormat += Lang.NameMonthsMin[Time.getMonth()]; break; // Сокращенное наименование месяца
            // Дни
            case 'd' : DateInFormat += zeroPlus(Time.getDate()); break;// День месяца
            case 'j' : DateInFormat += Time.getDate(); break; // День месяца без в.н.
            // Дни недели
            case 'N' : DateInFormat += Time.getDay() + 1; break; // Порядковый номер дня недели
                // case 'D' : DateInFormat += Lang.NameWeekdaysMin[Time.getDay()]; break; // Текстовое, сокращенное, представление дня недели
                // case 'L' : DateInFormat += Lang.NameWeekdays[Time.getDay()]; break; // Полное наименование дня недели
            // Минуты
            case 'i' : DateInFormat += zeroPlus(Time.getMinutes()); break; // с ведущим нулём
            // Секунды
            case 's' : DateInFormat += zeroPlus(Time.getSeconds()); break; // с ведущим нулём

            default : DateInFormat += format[i]; break;
        }
    }
    return DateInFormat;
}

function getDate(date) {
    if (date === "") return "";

    var dateObject = new Date(date);
    if (new Date().setHours(0, 0, 0, 0) < dateObject) {
        return parseDateFormat("G:i", dateObject);
    } else {
        return parseDateFormat("d.m.Y", dateObject);
    }
    return dateObject;
}
