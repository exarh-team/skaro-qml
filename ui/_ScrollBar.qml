import QtQuick 2.4

Item {
    id: container

    property bool stateVisible: false
    property variant scrollArea
	property real visibleSize: 8
    property int orientation: Qt.Vertical
	property bool traditional: true
	property bool drawBackground: true

	opacity: 0

	Component.onCompleted: {
		layout()
	}

    Timer {
        id: tim
        interval: 500
        running: true
        repeat: false
        onTriggered: stateVisible=false
    }

	function layout() {
		if (container.orientation == Qt.Vertical){
			container.anchors.top = scrollArea.top
			container.anchors.bottom = scrollArea.bottom
			container.anchors.right = scrollArea.right
			container.height = scrollArea.height
			container.width = container.visibleSize

		} else {
			container.anchors.left = scrollArea.left
			container.anchors.right = scrollArea.right
			container.anchors.bottom = scrollArea.bottom
			container.height = container.visibleSize
			container.width = scrollArea.width
		}
	}

    function position()
    {
		stateVisible=true
        tim.restart()

        var ny = 0
        if (container.orientation == Qt.Vertical) {
            ny = scrollArea.visibleArea.yPosition * container.height
        } else {
            ny = scrollArea.visibleArea.xPosition * container.width
        }
        if (ny > 2) {
            return ny
        } else {
            return 2
        }
    }

    function size()
    {
        var nh, ny

        if (container.orientation == Qt.Vertical)
            nh = scrollArea.visibleArea.heightRatio * container.height;
        else
            nh = scrollArea.visibleArea.widthRatio * container.width;

        if (container.orientation == Qt.Vertical)
            ny = scrollArea.visibleArea.yPosition * container.height;
        else
            ny = scrollArea.visibleArea.xPosition * container.width;

        if (nh < 10) {
            nh = 10
        }

        if (ny > 3) {
            var t

            if (container.orientation == Qt.Vertical)
                t = Math.ceil(container.height - 3 - ny);
            else
                t = Math.ceil(container.width - 3 - ny);

            if (nh > t) {
                return t
            } else {
                return nh
            }
        } else {
            return nh + ny
        }
    }

    Rectangle {
		id: scrollBorder
		radius: 3
		opacity: 0.7
		color: "black"
        x: 2
        width: container.width - 4
        y: 2
        height: container.height - 4

		MouseArea {
			id: mouseArea
			anchors.fill: parent
		}

		Binding {
			target: scrollBorder; property: "x"; value: position()
			when: container.orientation != Qt.Vertical
		}
		Binding {
			target: scrollBorder; property: "y"; value: position()
			when: container.orientation == Qt.Vertical
		}
		Binding {
			target: scrollBorder; property: "width"; value: size()
			when: container.orientation != Qt.Vertical
		}
		Binding {
			target: scrollBorder; property: "height"; value: size()
			when: container.orientation == Qt.Vertical
		}

    }

    states: [
        State {
            when: stateVisible
            PropertyChanges {
                target: container
                opacity: 1.0
            }
        },
        State {
            when: !stateVisible
            PropertyChanges {
                target: container
                opacity: 0
            }
        }
    ]
    transitions: Transition {
        NumberAnimation {
            property: "opacity"
            duration: 500
        }
    }
}
