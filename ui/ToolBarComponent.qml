import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0
import QtGraphicalEffects 1.0

ToolBar {
    property string selectDialog
    visible: pageLayout.visible

    Image {
        id: menuIcon
        visible: false
        source: Qt.resolvedUrl("../media/google-md/navigation-menu.svg")
        width: 24
        height: 24
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: searchIcon
        visible: false
        source: Qt.resolvedUrl("../media/google-md/action-search.svg")
        width: 24
        height: 24
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: closeIcon
        visible: false
        source: Qt.resolvedUrl("../media/google-md/navigation-close.svg")
        width: 24
        height: 24
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: arrowBackIcon
        visible: false
        source: Qt.resolvedUrl("../media/google-md/navigation-arrow-back.svg")
        width: 24
        height: 24
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: moreVertIcon
        visible: false
        source: Qt.resolvedUrl("../media/google-md/navigation-more-vert.svg")
        width: 24
        height: 24
        sourceSize: Qt.size(width, height)
    }

    RowLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: isMobile
            Layout.fillHeight: true
            Layout.preferredWidth: isMobile ? -1 : 300

            ToolButton {

                ColorOverlay {
                    width: menuIcon.width
                    height: menuIcon.height
                    source: menuIcon
                    color: "#fff"
                    anchors.centerIn: parent
                    visible: !((selectDialog !== "") && isMobile)
                }

                ColorOverlay {
                    width: arrowBackIcon.width
                    height: arrowBackIcon.height
                    source: arrowBackIcon
                    color: "#fff"
                    anchors.centerIn: parent
                    visible: (selectDialog !== "") && isMobile
                }

                onClicked: {
                    if ((selectDialog !== "") && isMobile) {
                        pageView.pop()
                        selectDialog = ""
                    } else {
                        drawer.open()
                    }
                }
            }

            Item {
                Layout.fillWidth: true


                Label {
                    font.pixelSize: 20
                    color: "#fff"
                    text: "Skaro"
                    opacity: !isSearch && ((selectDialog === "") || !isMobile)
                    anchors.verticalCenter: parent.verticalCenter
                }

                AvatarBase {
                    width: 32
                    height: 32
                    text: (selectDialog !== "") ? usersData[selectDialog].name : ""
                    opacity: (selectDialog !== "") && isMobile
                    radius: width/2
                    anchors.verticalCenter: parent.verticalCenter
                }

                Label {
                    font.pixelSize: 20
                    color: "#fff"
                    text: (selectDialog !== "") ? usersData[selectDialog].name : ""
                    opacity: (selectDialog !== "") && isMobile
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 42
                }

                TextField {
                    id: cl_search
                    placeholderText: qsTr("Search...")
                    opacity: isSearch
                    width: parent.width
                    anchors.verticalCenter: parent.verticalCenter
                    bottomPadding: topPadding

                    background: Rectangle {
                        color: cl_search.enabled ? "transparent" : "#ffffff"
                        border.color: "transparent"
                    }

                    Behavior on opacity {
                        NumberAnimation {
                            duration: opacitySearchTime
                        }
                    }

                    onTextChanged: {
                        if (cl_search.text !== "") {
                            root.searchByLogin(cl_search.text);
                        } else {
                            root.searchModel.clear();
                        }
                    }
                }
            }

            ToolButton {
                ColorOverlay {
                    width: searchIcon.width
                    height: searchIcon.height
                    source: searchIcon
                    color: "#fff"
                    opacity: !isSearch
                    anchors.centerIn: parent

                    Behavior on opacity {
                        NumberAnimation {
                            duration: opacitySearchTime
                        }
                    }
                }

                ColorOverlay {
                    width: closeIcon.width
                    height: closeIcon.height
                    source: closeIcon
                    color: "#fff"
                    opacity: isSearch
                    anchors.centerIn: parent

                    Behavior on opacity {
                        NumberAnimation {
                            duration: opacitySearchTime
                        }
                    }

                    Keys.onPressed: {
                        if ( event.key === Qt.Key_Enter || event.key === Qt.Key_Return ) {
                            root.search(cl_search.text)
                        }
                    }
                }

                onClicked: {
                    if (isSearch) {
                        isSearch = !isSearch
                    } else {
                        Qt.inputMethod.commit();
                        Qt.inputMethod.hide();
                        if (cl_search.text !== "") {
                            cl_search.cursorPosition = 0;
                            cl_search.text = "";
                        }
                        isSearch = !isSearch;
                    }
                }
            }
        }

        // two tab
        RowLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            visible: !isMobile

            ToolButton {
                ColorOverlay {
                    width: arrowBackIcon.width
                    height: arrowBackIcon.height
                    source: arrowBackIcon
                    color: "#fff"
                    anchors.centerIn: parent
                }

                onClicked: {
                    pageView.pop()
                }
            }

            AvatarBase {
                width: 32
                height: 32
                text: (selectDialog !== "") ? usersData[selectDialog].name : ""
                opacity: (selectDialog !== "")
                radius: width/2
            }

            Label {
                font.pixelSize: 20
                color: "#fff"
                text: (selectDialog !== "") ? usersData[selectDialog].name : ""
                opacity: (selectDialog !== "")
                anchors.verticalCenter: parent.verticalCenter
            }

            Item {
                Layout.fillWidth: true
            }

            ToolButton {
                ColorOverlay {
                    width: moreVertIcon.width
                    height: moreVertIcon.height
                    source: moreVertIcon
                    color: "#fff"
                    anchors.centerIn: parent
                }
            }
        }

    }
}
