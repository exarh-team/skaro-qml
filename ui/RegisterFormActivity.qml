import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

Item {

    ColumnLayout {
        y: (root.height-height)/2
        x: (root.width-root.width/15 < 300) ? root.width/30 : root.width/2-width/2
        width: (root.width-root.width/15 < 300) ? root.width-root.width/15 : 300

        TextField {
            placeholderText: qsTr("E-Mail")
            id: rf_email
            Layout.fillWidth: true
        }

        TextField {
            placeholderText: qsTr("User name")
            id: rf_login
            Layout.fillWidth: true
        }

        TextField {
            placeholderText: qsTr("Password")
            id: rf_password
            echoMode: TextInput.Password
            Layout.fillWidth: true
        }

        Button {
            text: "Register"
            highlighted: true
            Layout.fillWidth: true

            onClicked: {
                root.account_login = rf_login.text
                root.account_password = rf_password.text
                root.register(rf_email.text, rf_login.text, rf_password.text)

            }
        }

        Button {
            text: "Already have an account?"
            Layout.fillWidth: true

            onClicked: {
                loginView.pop()
            }

        }

    }

}
