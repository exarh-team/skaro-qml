import QtQuick 2.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: tab

    property string title

    property string iconSource

    property bool active: false

    signal clicked

    height: 48

    color: tab.active ? component.colorActive : component.colorInactive

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: {
            tab.clicked();
        }
    }

    Image {
        id: iconOrigin
        visible: false
        source: iconSource
        sourceSize: Qt.size(24 , 24)
    }

    ColorOverlay {
        id: icon
        width: iconOrigin.width
        height: iconOrigin.height
        source: iconOrigin
        color: tab.active ? "#00796b" : "#636363"
        anchors.horizontalCenter: parent.horizontalCenter

        Behavior on anchors.bottomMargin {
            NumberAnimation {
                duration: 200
            }
        }
    }

    Label {
        id: label
        text: title
        width: tab.width
        maximumLineCount: 2
        elide: Text.ElideRight
        wrapMode: Text.Wrap
        color: tab.active ? component.colorInactive : component.colorActive
        font.pixelSize: 14
        horizontalAlignment: Text.AlignHCenter
    }

    Behavior on color {
        ColorAnimation {
            duration: 200
        }
    }

    state: "titleOnly"
    states: [
        State {
            name: "titleOnly"
            when: title && !iconSource

            PropertyChanges {
                target: tab
                height: 48
            }

            PropertyChanges {
                target: label
                anchors.centerIn: tab
            }
        },
        State {
            name: "titleAndIcon"
            when: iconSource && title

            PropertyChanges {
                target: tab
                height: 56
            }

            PropertyChanges {
                target: label
                anchors.bottom: tab.bottom
                anchors.bottomMargin: 6
            }

            PropertyChanges {
                target: icon
                anchors.bottom: label.top
                anchors.bottomMargin: 3
            }
        },
        State {
            name: "iconOnly"
            when: iconSource && !title

            PropertyChanges {
                target: tab
                height: 48
            }

            PropertyChanges {
                target: icon
                anchors.centerIn: tab
            }

        }

    ]

}

