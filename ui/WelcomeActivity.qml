import QtQuick 2.4
import QtQuick.Controls 2.0

Item {
    width: root.width
    height: root.height

    BusyIndicator {
        running: true
        anchors.centerIn: parent
    }
}
